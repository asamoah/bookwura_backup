<!DOCTYPE html>

<html>

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>BookWura</title>
	<link rel="icon" href="{{ url('/images/bookwura_no_back.png') }}">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/search.css">
	<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
	 <link href="https://fonts.googleapis.com/css?family=Jaldi|Pacifico|Raleway" rel="stylesheet"> 

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108828016-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-108828016-1');
	</script>

</head>

<body>
<!-- NAVIGATION BAR BEGINS -->
<nav class="navbar">
	<div class="container">

<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN BEGINS -->
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"
	>
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN ENDS -->

<!-- NAME OF SITE BEGINS -->
	<a class="navbar-brand" href="index.php"><img src="{{ url('/images/bookwura_logo.jpg') }}"></a>
<!-- NAME OF SITE ENDS -->

	<div class="navbar-collapse collapse">

		<ul class="nav navbar-nav navbar-right">
		<!-- EXAMPLE OF DROPDOWN MENU IN THE NAVIGATION BAR BEGINS -->
			<li><a href="index.php">Home</a></li>

			<li><p class="navbar-btn"><a href="search.php" class="btn btn-primary">Search For Book By Title</a></p></li>
			

		</ul>
	</div>

	</div>
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-5">
				<img src="images/login.png" class="pull-right" width="300px">
			
		</div>

		<div class="col-md-offset-2 col-md-4">
			 <div class="page-header">Sell A Book</div>

            <small>Fill the Form Below to Sell Your Book.</small>
          
          
                       <form action="register.php" method="post">
							<input type="text" class="form-control" name="title" placeholder="Type In The Title of Your Book*">
							<br>
							  <select name="condition_of_book" class="form-control">
		 							<option>My Book is...</option>
		 							<option value="Used">Used</option>
		 							<option value="New">New  </option>
		 							<option value="Old">Old  </option>
							  </select>
							<br>
							<textarea name="description" class="form-control" placeholder="Give A Brief Description such as Edition, etc."></textarea>
							<br>
							<input type="num" name="price" class="form-control" placeholder="At What Price Do You Want To Sell The Book?*">
							
							<br>
								<label>Please Click The Button to Select A Picture of Your Book: </label>
								<input type="file" name="picture">
								<br>

								 <input  class='form-control' type='text' name='user_name' placeholder='Type your username'>
                            <br>
                            
                           
                         
                            <input class='form-control' type='password' name='pass_word' placeholder="type your password here">
                            <br>

									
							 <input type='submit' class="btn btn-primary" name='submit' value='Post'>
					</form>
						<small>You need to <a href="register.php">Sign Up</a>  to Sell A Book</small>
                	<br><br> 
		
		</div>
	</div>
</div>
<br><br><br><br><br><br><br><br>

<div class="container-fluid" id="foot">

	<div class="row" style="margin:0; padding:2em 0 0 0">
		<div class="col-md-3">
			<h4>&copy PraXis Technologies, 2017</h4>
		</div>

		<div class="col-md-6">
			<ul>
				<li><a>FAQs</a></li>
				<li><a>Contact Us</a></li>
				<li><a>Locate Us</a></li>
			</ul>
		</div>

		<div class="col-md-3">
			<p class="pull-right">ALL RIGHTS RESERVED</p>
		</div>
	</div>
   
</div>

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   
    <script src="{{ url('f-style/js/bootstrap.min.js') }}"></script>
</body>

</html>