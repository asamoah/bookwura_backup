<!DOCTYPE html>
<html>
<head>
  <title>{{ config('app.codename')}}</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{ url('back/css/vendor.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ url('back/css/flat-admin.css') }} ">

  <!-- Theme -->
  <link rel="stylesheet" type="text/css" href="{{ url('back/css/theme/blue-sky.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ url('back/css/theme/blue.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ url('back/css/theme/red.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ url('back/css/theme/yellow.css') }} ">

</head>
<body>
@yield('content')
      
<!--
  <script type="text/javascript" src="{{ url('back/js/vendor.js') }} "></script>
  <script type="text/javascript" src="{{ url('back/js/app.js') }} "></script>
-->

</body>
</html>