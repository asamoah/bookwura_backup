<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ config('app.name')}}</title>
	<link rel="icon" href="{{ url('/images/bookwura_no_back.png') }}">

	@yield('add-head')

	<link href="{{ url('f-style/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ url('f-style/css/font-awesome/css/font-awesome.min.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Jaldi|Pacifico|Raleway" rel="stylesheet">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108828016-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-108828016-1');
	</script>

</head>
<style type="text/css">

	#foot li {
		margin: 1em;
		display: inline;
	}
	#soc-icon li {
		margin: 0;
		display: inline;
	}
	#foot li a{
		font-size: 15px;
		color: white;
	}
</style>
<body>

@yield('top-nav')
@yield('top-menu')
<div class="container">
	<div class="row">
		<!-- <div class="col-sm-1 col-sm-offset-1">
			<br><br><br><br><br>
			@yield('left-ads')
		</div> -->

		<!-- <div class="col-md-6 col-md-offset-3" > -->
			@yield('content')

		<!-- </div> -->
		<!-- <div class="col-md-1 col-md-offset-1">
			<br><br><br><br><br>
			@yield('right-ads')
		</div>	 -->

	</div>

	<div class="row" style="margin:1.5em 0 2em 0;">
		<center>
			@yield('ads')
		</center>
		
	</div>

</div>



<div class="row" id="foot" style="margin:0; padding:2em 0 0 0">
	<center>
	<div class="col-md-3">
		<h6>&copy PraXis Technologies, 2017</h6>
	</div>

	<div class="col-md-6" style="text-align:left">
		<ul>
			<li><a>FAQs</a></li>
			<li><a>Contact Us</a></li>
			<li><a>Locate Us</a></li>
		</ul>
	</div>

	<div class="col-md-3" id="soc-icon">
		<ul>
			<li><a href=""><img src="{{ url('/soc-icon/facebook.png')}}" style="width: 2em; height: 2em"></a></li>
			<li><a href=""><img src="{{ url('/soc-icon/twitter.png')}}" style="width: 2em; height: 2em"></a></li>
			<li><a href=""><img src="{{ url('/soc-icon/instagram.png')}}" style="width: 2em; height: 2em"></a></li>
			<!-- <li><p class="navbar-btn"><a href="{{ url('/sell')}}" class="btn btn-primary">Sell A Book</a></p></li> -->

		</ul>
		<p class="">ALL RIGHTS RESERVED</p>
	</div>
</center>
</div>

<!-- </div> -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{ url('f-style/js/bootstrap.min.js') }}"></script>
</body>

</html>
