@extends('back.dash-master')

@section('content')

@if(count($errors))
  @include('back.partials.error-sec')
@elseif($success_msg)
  @include('back.partials.success-sec')
@endif
@include('back.partials.tiny')

<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="card">
      <div class="card-header">
        All Registered Sellers
      </div>
      <div class="card-body no-padding">
        <table class="datatable table table-striped primary" width="100%">
          <thead> 
              <tr>
                  <th>N0#</th>
                  <th>Name</th>
                  <th>Books</th>
                  <th>Created At</th>
                  <th>Updated At</th>
                  <th></th>
              </tr>
          </thead>
          <tbody>
          @if($sel)
          @php($count = 1)
            @foreach($sel as $pt)
              @if($pt->status === 0)
                @php($bord = 'color:green')
              @else
                @php($bord = 'color:red')
              @endif

              <tr>
                  <td style="{{ $bord }}" >{{ $count }}</td>
                  <td>{{ $pt->name }}</td>
                  <td>{{ $pt->books->count() }}</td>
                  <td>{{ $pt->created_at->toDayDateTimeString() }}</td>
                  <td>{{ $pt->updated_at->toDayDateTimeString() }}</td>
                  <td>

                    @if($pt->status === 0)
                      <a href="{{ url('/seller-state/1/'.$pt->id) }}" class="btn btn-xs btn-danger">DEL</a>
                    @else
                      <a href="{{ url('/seller-state/0/'.$pt->id) }}" class="btn btn-xs btn-success">ACT</a>
                    @endif
                    <button class="btn btn-xs btn-warning open-modal" data-toggle="modal" data-target="#myModal" value="{{$pt->id}}" >VIEW</button>
                  </td>
              </tr>
              @php($count++)
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Seller Information</h4>
            </div>
            <div class="modal-body">
                    <p class="label" id='sender'>npne</p><br><br>
                    <p class="label" id='username'>npne</p><br><br>
                    <p class="label" id='email'>npne</p><br><br>
                    <p class="label" id='phone'>npne</p><br><br>
                    <p class="label" id='location'>npne</p>
                    <p class="label" id='created'>npne</p>
                    <!-- <span class="label" id='created'>one</span> -->
            </div>
            <div class="modal-footer">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> CLOSE </button>
                <input type="hidden" id="task_id" name="task_id" value="0">
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        var url = "{{ url('/seller-view') }}";

        //display modal form for task editing
        $('.open-modal').click(function(){
            var task_id = $(this).val();

            $.get(url + '/' + task_id, function (data) {
                //success data
                // console.log(data);
                // var img_link = 
                // $('#editing').attr('href', "{{ url('/agcDivi_edit') }}" + '/'+ data.id);
                // $('#myModalLabel').html(data.name);
                // $('#mess').html(data.description);
                // $('#featImg').attr('src',"{{ url('/')}}"+data.foto);
                // $('#div_id').val(data.id);
                $('#username').html('username : ' + data.username);
                $('#sender').html('name : ' + data.name);
                $('#phone').html('phone : ' + data.phone);
                $('#email').html('email : ' + data.email);
                $('#location').html('email : ' + data.location);
                $('#created').html('created on : ' + data.created_at);
                // $('#task').val(data.task);
                // $('#description').val(data.description);
                // $('#btn-save').val("update");

                // $('#myModal').modal('show');
            }) 
        });
    });
</script>
@endsection