@extends('back.dash-master')

@section('content')

@if(count($errors))
  @include('back.partials.error-sec')
@elseif($success_msg)
  @include('back.partials.success-sec')
@endif
@include('back.partials.tiny')
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="card">
      <!-- <button style="margin: 2em;">Add Work Experince</button> -->
      <div class="card-header">
        All Posted Ads
      </div>
      <div class="card-body row">

      @php($pro_p = \App\Advert::orderBy('updated_at','DESC')->paginate(9))

      @if($pro_p->count() == 0)
        <i>No Other Profile Picture Available</i>

      @else
      @foreach($pro_p as $pp)
        <div class="col-xs-4" style="margin:0 0 2em 0">
          <p>{{ $pp->name }}</p>
          <img src="{{ url($pp->foto) }}" style="width:100%; height:175px;" class="img-responsive img-thumbnail">

          <center>
          @if($pp->status == 0)
            <a href="{{ url('/ad-state/1/'.$pp->id) }}" class="btn-warning btn-sm" style="margin:0;">make In-Active</a>
          @else
            <a href="{{ url('/ad-state/0/'.$pp->id) }}" class="btn-success btn-sm" style="margin:0;">make Active</a>
          @endif
          </center>
        </div>
      
      @endforeach
      </div>
      <div class="card-footer">
        {{ $pro_p->links() }}
      </div>

      @endif
    </div>
  </div>
</div>
@endsection