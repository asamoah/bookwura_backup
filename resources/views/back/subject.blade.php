@extends('back.dash-master')

@section('content')

@if(count($errors))
  @include('back.partials.error-sec')
@elseif($success_msg)
  @include('back.partials.success-sec')
@endif
<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="card">
        <div class="card-header">
          Add Book Subject
        </div>
        <div class="card-body">
          <form method="post" action="{{ route('new_subject') }}">
          {{ csrf_field() }}
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Subject</span>
                <input type="text" name="title" class="form-control" placeholder="title" aria-describedby="basic-addon1">
              </div>
            </div>
            <hr>
            <div class="row form-group">
              <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
  </div>
<!-- </div> -->

  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="card">
      <div class="card-header">
        All Available Subject
      </div>
      <div class="card-body no-padding">
        <table class="datatable table table-striped primary" width="100%">
          <thead> 
              <tr>
                  <th>N0#</th>
                  <th>Title</th>
                  <th></th>
              </tr>
          </thead>
          <tbody>
          @if($ct)
          @php($count = 1)
            @foreach($ct as $pt)
              @if($pt->status === 0)
                @php($bord = 'color:green')
              @else
                @php($bord = 'color:red')
              @endif
              <tr>
                  <td  style="{{ $bord }}" >{{ $count }}</td>
                  <td>{{ $pt->title }}</td>
                  <td>

                  @if($pt->status === 0)
                    <a href="{{ url('/subject-state/1/'.$pt->id) }}" class="btn btn-xs btn-danger">DEL</a>
                  @else
                    <a href="{{ url('/subject-state/0/'.$pt->id) }}" class="btn btn-xs btn-success">ACT</a>
                  @endif
                  
                  </td>
              </tr>
              @php($count++)
            @endforeach
          @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection
