<div class="">
    <div class="alert alert-danger" role="alert">
        <ul>
        @if(count($errors))
        @foreach($errors->all()  as $err)
            <li>{{ $err }}</li>
        @endforeach
            @endif
        </ul>
    </div>
</div>