@extends('back.dash-master')

@section('content')

@if(count($errors))
  @include('back.partials.error-sec')
@elseif($success_msg)
  @include('back.partials.success-sec')
@endif

<script>
  var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>


<div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12">
    <div class="card">
        <div class="card-header">
          Add New Advertisement Pic
        </div>
        <div class="card-body">
          <form id="form1" method="post" action="{{ route('post_ad') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">NAME</span>
                  <input type="text" name="name" class="form-control" placeholder="title of advertisement" aria-describedby="basic-addon1">
                </div>
                <div class="input-group">
                  <span class="input-group-addon" id="basic-addon1">FEATURED iMAGE</span>
                  <input type="file" id="imgInp" name="feat_img" class="form-control" placeholder="Input group" aria-describedby="basic-addon1" accept="image/*" onchange="loadFile(event)">
                </div>
              </div>
              <!-- </div> -->
              <div class="input-group">
                <img id="output" src="#" alt="No Image Selected" style="width:100%; height: 330px" /> 
              </div>
              <hr>
              <div class="row form-group">
                <div class="col-md-9 col-md-offset-3">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"> CLOSE </button>
                </div>
              </div>
            </form>
        </div>
      </div>
  </div>
<!-- </div> -->
</div>
@endsection
