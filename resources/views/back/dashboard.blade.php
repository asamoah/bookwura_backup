@extends('back.dash-master')

@section('content')

<!-- <div class="row">
  <div class="col-xs-12">
    <div class="card card-banner card-chart card-green no-br">
      <div class="card-body">
        <div class="ct-chart-sale"></div>
        Welcome to BookWura
      </div>
    </div>
  </div>
</div> -->
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <a class="card card-banner card-orange-light">
        <div class="card-body">
          <i class="icon fa fa-bookmark-o "></i>
          <div class="content">
            <div class="title">Welcome to BookWura</div>
            <!-- <div class="value">{{ \App\Book::count() }}</div> -->
          </div>
        </div>
      </a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-green-light">
        <div class="card-body">
          <i class="icon fa fa-book fa-4x"></i>
          <div class="content">
            <div class="title">Books Available</div>
            <div class="value">{{ \App\Book::count() }}</div>
          </div>
        </div>
      </a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-blue-light">
        <div class="card-body">
          <i class="icon fa fa-users fa-4x"></i>
          <div class="content">
            <div class="title">Registered Sellers</div>
            <div class="value"> {{ \App\Seller::count() }}</div>
          </div>
        </div>
      </a>

  </div>
  <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
      <a class="card card-banner card-yellow-light">
        <div class="card-body">
          <i class="icon fa fa-picture-o fa-4x"></i>
          <div class="content">
            <div class="title">Posted Ads</div>
            <div class="value">{{ \App\Advert::count() }}</div>
          </div>
        </div>
      </a>

  </div>
</div>

@endsection