@extends('layouts.auth-master')

@section('content')
  <div class="app app-default">
<div class="app-container app-login">
  <div class="flex-center">
    <div class="app-header"></div>
    <div class="app-body">
      <div class="loader-container text-center">
          <div class="icon">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
              </div>
            </div>
          <div class="title">Logging in...</div>
      </div>
      <div class="app-block">
          
        <div class="app-right-section">
          <div class="app-brand">{{ config('app.codename') }}</div>
          <div class="app-info">
            
            <ul class="list">
              <li>
                <div class="icon">
                  <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                </div>
                <div class="title">Increase <b>Productivity</b></div>
              </li>
              <li>
                <div class="icon">
                  <i class="fa fa-cubes" aria-hidden="true"></i>
                </div>
                <div class="title">Lot of <b>Components</b></div>
              </li>
              <li>
                <div class="icon">
                  <i class="fa fa-key" aria-hidden="true"></i>
                </div>
                <a class="title" href="{{ route('login')}}">Have an Account ?<b>Sign In</b></a>
              </li>
            </ul>
          </div>
        </div>
        <div class="app-form">
            <div class="">
        @if(count($errors))
                @include('back.partials.error-sec')
        @endif
        @if($success_msg)
                @include('back.partials.success-sec')
        @endif
          <div class="form-suggestion">
            All Fields Are Required.
          </div>
          <form action="{{ route('register') }}" method="post">
              {{ csrf_field() }}
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">
                  <i class="fa fa-paper-plane" aria-hidden="true"></i></span>
                <input type="text" class="form-control" name="name" placeholder="Fullname" aria-describedby="basic-addon1">
              </div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon2">
                  <i class="fa fa-user" aria-hidden="true"></i></span>
                <input type="text" class="form-control" name="username" placeholder="Username" aria-describedby="basic-addon2">
              </div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon2">
                  <i class="fa fa-envelope" aria-hidden="true"></i></span>
                <input type="email" class="form-control" name="email" placeholder="email" aria-describedby="basic-addon2">
              </div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon3">
                  <i class="fa fa-key" aria-hidden="true"></i></span>
                <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon3">
              </div>
              <div class="input-group">
                <span class="input-group-addon" id="basic-addon4">
                  <i class="fa fa-check" aria-hidden="true"></i></span>
                <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" aria-describedby="basic-addon4">
              </div>
              <div class="text-center">
                  <input type="submit" name="send" class="btn btn-success btn-submit" value="Register">
              </div>
          </form>
        </div>
      </div>
    </div>
    <div class="app-footer">
    </div>
  </div>
</div>

  </div>
  
@endsection