@extends('layouts.auth-master')

@section('content')
  <div class="app app-default">

<div class="app-container app-login">
  <div class="flex-center">
    <div class="app-header"></div>
    <div class="app-body">
      <div class="loader-container text-center">
          <div class="icon">
            <div class="sk-folding-cube">
                <div class="sk-cube1 sk-cube"></div>
                <div class="sk-cube2 sk-cube"></div>
                <div class="sk-cube4 sk-cube"></div>
                <div class="sk-cube3 sk-cube"></div>
              </div>
            </div>
          <div class="title">Logging in...</div>
      </div>
      <div class="app-block">
      <div class="app-form">
      @if(count($errors))
            @include('back.partials.error-sec')
        @endif
        @if($success_msg)
                @include('back.partials.success-sec')
        @endif
        <div class="form-header">
          <div class="app-brand">{{ config('app.codename')}}</div>
        </div>
        <form action="{{ route('back-login') }}" method="POST">
            {{ csrf_field() }}
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1">
                <i class="fa fa-envelope" aria-hidden="true"></i></span>
              <input type="email" class="form-control" name="email" placeholder="email" aria-describedby="basic-addon1">
            </div>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon2">
                <i class="fa fa-key" aria-hidden="true"></i></span>
              <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon2">
            </div>
            <div class="text-center">
                <input type="submit" class="btn btn-success btn-submit" value="Login">
            </div>
        </form>

        <div class="form-line">
          <div class="title">OR</div>
        </div>
        <div class="form-footer">
            <a href="{{ route('back-register')}}">Dont have an Account ? ..Sign Up</a>
        </div>
      </div>
      </div>
    </div>
    <div class="app-footer">
    </div>
  </div>
</div>

  </div>
@endsection