@component('mail::message')
# New Backend User

Dear Admin,

A new user has register to be an administrtor of the System
Please go online and activate the user.
The User Details is Below
 - name : {{ $last_mes ->name}}
 - username : {{ $last_mes ->usename}}
 - email : {{ $last_mes ->email}}

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
