@extends('layouts.front-master')

@section('add-head')
	<link rel="stylesheet" type="text/css" href="{{ url('f-style/css/search.css') }}">
	<style type="text/css">

	#stats li {	}
</style>
@endsection

@section('top-nav')
	@include('front.partials.top-nav-one')
@endsection

<!-- @section('left-ads')
	@include('front.partials.left-ads')
@endsection -->

@section('content')
<div class="col-md-6 col-md-offset-3" >
	<br><br><br><br><br><br><br><br>
	@if(count($errors))
	    <div class="alert alert-danger">
	        @foreach($errors->all()  as $err)
	            <li>{{ $err }}</li>
	        @endforeach
	    </div>
	@endif
	<h1 class="text-center">Search For Book By Title</h1>

	<form method="post" action="{{ route('search_bk') }}">
		{{ csrf_field() }}
		<!-- <div class="input-group"> -->
			<div class="form-group">
				<input type="text" name="search" class="form-control" >
			</div>
		<center>
			<div class="row">
				<div class="col-md-offset-1 col-md-5">
					<span class="input-group-btn">
						<button class="btn btn-primary butn text-center" style=" padding: 0.4em 3em">Search <i class="fa fa-search"></i></button>
					</span>
				</div>
				<div class="col-md-5">
					<span class="input-group-btn">
						<a href="{{ url('/sell')}}" class="btn btn-warning butn text-center" style=" padding: 0.4em 3em">Sell A Book <i class="fa fa-money"></i></a>
					</span>
				</div>
			</div>
		</center>
		<!-- </div> -->
	</form>
	<br>
		<div  id="stats" class="row">
			<center>
					<div class="col-xs-4">
						<h1>{{ \App\Book::active()->count() * 11 * \App\Seller::active()->count() }}</h1>
						<!-- <p><a href="{{ url('/all-books')}}">BOOKS AVAILABLE</a></p> -->
						<p>BOOKS AVAILABLE</p>
					</div>
					<div class="col-xs-4">
						<h1>{{ \App\Subject::active()->count() }}</h1>
						<!-- <p><a href="{{ url('/all-books-carte/1')}}">SUBJECT AREAS</a></p> -->
						<p>SUBJECT AREAS</p>
					</div><div class="col-xs-4">
						<h1>{{ \App\Seller::active()->count() *11 - 9 }}</h1>
						<p>SELLERS</p>
					</div>
			</center>
		</div>
	</div>
@endsection

@section('ads')
	@include('front.partials.left-ads')
@endsection
