@extends('layouts.front-master')

@section('add-head')
	<link rel="stylesheet" type="text/css" href="{{ url('f-style/css/search.css') }}">
@endsection

@section('top-nav')
	@include('front.partials.top-nav-two')
@endsection

@section('top-menu')
	<div class="container">
		@if(count($errors))
		    <div class="alert alert-danger">
		        @foreach($errors->all()  as $err)
		            <li>{{ $err }}</li>
		        @endforeach
		    </div>
		@endif
		@if ($success_msg)
		    <div class="alert alert-success">
		        {{ $success_msg }}
		    </div>
		@endif

		<div class="row">
			<div class="col-md-5">
					<img src="{{ url('/form.jpg') }}" width="300px">
			</div>

			<div class="col-md-offset-2 col-md-4">
				<form action="{{ route('new_book') }}" method="post" enctype="multipart/form-data">
				<div class="page-header">Sell A Book</div>
				<small>You need to <a href="{{ url('/signup')}}">Sign Up</a>  to Sell A Book. </small>
				<br>
				<small>You may proceed if You Already have an Account</small>
				<br>
				<input  class='form-control' type='text' name='username' placeholder='Type your username'>
                <br>
                 <input class='form-control' type='password' name='password' placeholder="type your password here">
                <hr>

            	<small>Fill the Form Below to Sell Your Book.</small>
                <!-- <form action="{{ route('new_book') }}" method="post" enctype="multipart/form-data"> -->
                	{{ csrf_field() }}
					<input type="text" class="form-control" name="title" placeholder="Type In The Title of Your Book*">
					<br>
				    <select name="carte" class="form-control">
						<option>My Book is...</option>
						@php($ct = \App\Carte::active()->latest()->get())
						@foreach($ct as $ct)
							<option value="{{$ct->id}}">{{ $ct->title }}</option>
						@endforeach
				    </select>
					<br>
				    <select name="subject" class="form-control">
						<option>Please Select A Subject</option>
						@php($ct = \App\Subject::active()->latest()->get())
						@foreach($ct as $ct)
							<option value="{{$ct->id}}">{{ $ct->title }}</option>
						@endforeach
				    </select>
					<br>
					<textarea name="description" class="form-control" style="min-height:170px" placeholder="Give A Brief Description such as Edition, etc."></textarea>
					<br>
					<input type="num" name="price" class="form-control" placeholder="At What Price Do You Want To Sell The Book?*">

					<br>
					<label>Please Click The Button to Select A Picture of Your Book: </label>
					<input type="file" name="feat_img">
					<br>
					<hr>

					


					<input type='submit' class="btn btn-primary" name='submit' value='Post'>
					</form>
					<br>
					

                	<br><br>
			</div>
		</div>
	</div>
<br><br><br><br><br><br><br><br>
@endsection
