<div class="navbar navbar-default" style="margin-bottom: 0px;padding: 0 0 1em 0;">
	<div class="container">

<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN BEGINS -->
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN ENDS -->

<!-- NAME OF SITE BEGINS -->
		<a class="navbar-brand" href="{{ url('/home')}}"><img src="{{ url('/images/bookwura_no_back.png') }}" style="margin:0; width: 150px; height: 35px;"></a>
	<!-- NAME OF SITE ENDS -->

		<div class="navbar-collapse collapse">

			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="{{ url('/home')}}">Home</a>
				</li>

				<li>
					<p class="navbar-btn">
						<a href="{{ url('/home')}}" class="btn btn-primary">Search For Book By Title</a>
					</p>
				</li>
			</ul>
		</div>

	</div>

</div>
