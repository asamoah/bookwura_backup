@extends('layouts.front-master')

@section('add-head')
	<link rel="stylesheet" type="text/css" href="{{ url('f-style/css/search.css') }}">
@endsection

@section('top-nav')
	@include('front.partials.top-nav-two')
@endsection

@section('top-menu')
	<div class="container">

		@if(count($errors))
		    <div class="alert alert-danger">
		        @foreach($errors->all()  as $err)
		            <li>{{ $err }}</li>
		        @endforeach
		    </div>
		@endif
		@if ($success_msg)
		    <div class="alert alert-success">
		        {!! $success_msg !!}
		    </div>
		@endif
		<div class="row">
			<div class="col-md-5">
					<img src="{{ url('/form.jpg') }}" class="pull-right" width="300px">
			</div>

			<div class="col-md-offset-2 col-md-4">
				<form action="{{ route('new_seller') }}" method="post">
			 	<div class="page-header">Register Here</div>

            	<small>Sign Up to Sell An Unlimited Number of Books!</small>
            	<hr>
          		<!-- <form action="{{ route('new_seller') }}" method="post"> -->
          			{{ csrf_field() }}
					<input type="text" class="form-control" name="name" placeholder="surname , firstname othername">
							<br>
					<input type="text" class="form-control" name="username" placeholder="Choose A Username*">
							<br>
					<input type="password" name="password" class="form-control" placeholder="Please Enter Your Password*">
							<br>
					<input type="password" name="password_confirmation" class="form-control" placeholder="Please Enter Your Password Again*">

							<br>
					<input type="tel" name="phone" class="form-control" placeholder="Please Enter Your Telephone Number*">
								<br>
					<input type="text" name="location" class="form-control" placeholder="where are you located">
								<br>
					<input type="email" name="email" class="form-control" placeholder="Please Enter Your email">
										<br>
					<input type='submit' class="btn btn-primary" name='submit' value='Register'>
				</form>
				<small>Already have an Account <a href="{{ url('/sell')}}">Sign In</a>  to Sell A Book</small>

            	<br><br>
			</div>
		</div>
	</div>
<br><br><br><br><br><br><br><br>
@endsection
