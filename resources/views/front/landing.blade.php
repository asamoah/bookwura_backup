<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="{{ url('/images/bookwura_no_back.png') }}">
	<title>{{ config('app.name')}}</title>
	<link href="{{ url('f-style/css/bootstrap.min.css') }}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ url('f-style/css/index.css') }}">
	<link rel="stylesheet" href="{{ url('f-style/css/font-awesome/css/font-awesome.min.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Jaldi|Pacifico|Raleway" rel="stylesheet">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108828016-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-108828016-1');
	</script>

</head>

<body>
<!-- NAVIGATION BAR BEGINS -->
<nav class="navbar">
	<div class="container">

<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN BEGINS -->
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"
	>
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	</button>
<!-- SUMMARY NAVBAR BUTTON FOR SMALLER SCREEN ENDS -->

<!-- NAME OF SITE BEGINS -->
<!-- <a class="navbar-brand" href="{{ url('/home')}}" style="color: white;"><img src="{{ url('/images/bookwura_png_white.png') }}" style="margin:0; width: 150px; height: 35px;"></a> -->
<!-- NAME OF SITE ENDS -->



	</div>
</nav>
<!-- NAVIGATION BAR ENDS -->


<div class="col-md-6 col-md-offset-3" style="margin-top: 0px;padding: 0; color: white">
<center style="font-size: 1.1em">
	<h3 style="font-size: 3em">Welcome <br><small>TO</small></h3>
	<!-- <img src="{{ url('/images/bookwura_png_white.png') }}" style="margin:0 0 1em 0; width: 100%; height: 105px;"> -->
	<h1 style="font-size: 4.5em;font-family: fantasy;">BookWura</h1>
	<p><b>Students want to buy your used books</b></p>
	<p>Make money and share knowledge as you sell your used books to the millions of individuals who need them. Our mission is simple; <b>connecting those who need books today with those who used them yesterday</b></p>
	<p><i>Dont throw away these used books - pass them on. Become a BookWura now !</i></p>
</center>

<a class="pull-right" style="margin:2em;font-size: 1.5em" href="{{ url('/home')}}">Click To Enter Website <span class="fa fa-arrow-right"></span></a>

</div>

<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<!-- insert these breaks -->

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="{{ url('f-style/js/bootstrap.min.js') }}"></script>
</body>

</html>
