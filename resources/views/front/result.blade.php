@extends('layouts.front-master')

@section('add-head')
	<link rel="stylesheet" type="text/css" href="{{ url('f-style/css/result.css') }}">
@endsection

@section('top-nav')
	@include('front.partials.top-nav-one')
@endsection

@section('top-menu')
	<div class="container " style="margin:1em 0 0 0;">
		<div class="col-md-6 col-md-offset-3">
			  <form method="post" action="{{ route('search_bk') }}">
				{{ csrf_field() }}
				<!-- <div class="input-group"> -->
					<div class="form-group">
						<input type="text" name="search" class="form-control" >
					</div>
				<center>
					<div class="row">
						<div class="col-md-offset-1 col-md-5">
							<span class="input-group-btn">
								<button class="btn btn-primary butn text-center" style=" padding: 0.4em 3em">Search <i class="fa fa-search"></i></button>
							</span>
						</div>
						<div class="col-md-5">
							<span class="input-group-btn">
								<a href="{{ url('/sell')}}" class="btn btn-warning butn text-center" style=" padding: 0.4em 3em">Sell A Book <i class="fa fa-money"></i></a>
							</span>
						</div>
					</div>
				</center>
				<!-- </div> -->
			</form>
		</div>
	</div>

	<br><br><br>
@endsection

<!-- @section('left-ads')
	@include('front.partials.left-ads')
@endsection -->

@section('content')
<!-- <div class="row" > -->
@if(isset($bk))
@php
$bkk =  \App\Book::where('books.name','LIKE',$bk)->join('sellers','books.seller_id','=','sellers.id')->join('cartes','books.carte_id','=','cartes.id')->select('books.*','sellers.name as sname','sellers.username as s_uname','sellers.phone','sellers.location','cartes.title as c_title')->paginate(8);

@endphp
@if($bkk->count() != 0 )
		<center>
			<h4>
				<span class="label label-default"> search for : <i>{{ explode('%',$bk)[1]}}</i></span>
			</h4>
		
			{{ $bkk->links() }}
		</center>
	@foreach($bkk as $bk)
	<div class="col-md-6" style="margin:0 0 1em 0;">
		<div class="media display" style="margin:0;box-shadow: 5px 5px 2.5px #888888;">
			<h4>{{ $bk->name }}</h4>
       		<div class="media-left">
	          <a href="#">
	            @if($bk->foto != null)
	            	<img class="media-object" src="{{ url($bk->foto) }}"  width="160px" height="135px" class="img-circle img-responsive" style="margin:0;">
	            @endif
	        </a>
	       	</div>
	       	<div class="media-body" style="padding:0;">

	              
	              <p>{{ $bk->price }}</p>
	              <p>{{ 'Condition: '.$bk->c_title }}</p>
	              <p>{!! 'seller :<wbr> '.$bk->sname !!}</p>
	              <p>{{ 'located At : '.$bk->location }}</p>
	              

	       	</div>
	       	<p style="margin-bottom: 0;">{!! 'Contact <mark>'.$bk->phone.'</mark> for more details'!!}</p>
	       	<p style="margin:0"><small>{{ 'posted :'.$bk->created_at->diffForHumans() }}</small></p>
		</div>
    </div>
		<!-- <div class="col-md-1"></div> -->
    @endforeach
		<center>
			{{ $bkk->links() }}
		</center>
@endif
@else
	<div class="alert alert-danger" style="text-align: center; margin-bottom: 18em">
		<h3>Please Enter Something in the Search Box</h3>
	</div>
@endif

<!-- </div> -->
@endsection

@section('ads')
	@include('front.partials.right-ads')
@endsection
