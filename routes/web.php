<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('flor')->group(function () {
    Route::get('/', function () {
    return view('welcome');
});
});

Route::get('/', function () {
    return view('front.landing');
});
Route::get('/home', function () {
    return view('front.search');
});
Route::get('/result', function () {
    return view('front.result');
});
Route::get('/all-books', function () {
    return view('front.show_all');
});

Route::get('/sell', function () {
	$success_msg = '';
    return view('front.sell', compact('success_msg'));
});
Route::get('/signup', function () {
	$success_msg= '';
    return view('front.signup', compact('success_msg'));
});

Route::get('/all-books-carte/{id}', 'SellController@sort_cartegory');
Route::post('/result', 'SellController@home_search')->name('search_bk');

Route::get('/phi','BackendController@index')->name('backend');
Route::get('/phi-login','BackendController@login')->name('login');
Route::post('/phi-login','LoginController@store')->name('back-login');
Route::get('/phi-logout','LoginController@destroy')->name('logout');
Route::get('/phi-register','BackendController@register')->name('back-register');
Route::post('/phi-register','RegisterController@index')->name('register');

Route::get('/list-sellers', 'SellerController@list_all')->name('all-sellers');
Route::post('/signup', 'SellerController@create')->name('new_seller');
Route::get('/seller-state/{id}/{eid}', 'SellerController@state_change');
Route::get('/seller-view/{id}', 'SellerController@single');

Route::get('/list-carte', 'CarteController@list_all')->name('all-carte');
Route::post('/carte', 'CarteController@create')->name('new_carte');
Route::get('/carte-state/{id}/{eid}', 'CarteController@state_change');

Route::get('/list-subject', 'SubjectController@list_all')->name('all-subject');
Route::post('/subject', 'SubjectController@create')->name('new_subject');
Route::get('/subject-state/{id}/{eid}', 'SubjectController@state_change');

Route::post('/sell', 'BookController@create')->name('new_book');
Route::get('/available-books', 'BookController@list_all')->name('books-available');
Route::get('/book-state/{id}/{eid}', 'BookController@state_change');
Route::get('/book-view/{id}', 'BookController@single');

Route::get('/advertisement', 'AdvertController@make_form')->name('ad_form');
Route::get('/my-advertisement', 'AdvertController@ads_list')->name('ad_list');
Route::get('/ad-state/{id}/{eid}', 'AdvertController@state_change');
Route::post('/advertisement', 'AdvertController@create')->name('post_ad');
// Route::get('/book-viewer/{id}', 'BookController@sing');