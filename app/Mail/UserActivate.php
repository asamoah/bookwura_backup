<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserActivate extends Mailable
{
    use Queueable, SerializesModels;
    
    public $last_mes;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($last_mes)
    {
        //
        $this->last_mes = $last_mes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New User Activation ')->markdown('emails.user.activation');
    }
}
