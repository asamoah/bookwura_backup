<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    public function scopeActive($query)
    {
        return $query->where('status', 0);
    }

   	public function seller()
    {
    	return $this->belongsTo(Seller::class);
    }
    public function carte()
    {
        return $this->belongsTo(Carte::class);
    }
    public function subject()
    {
    	return $this->belongsTo(Subject::class);
    }
}
