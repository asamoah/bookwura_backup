<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    //
    public function scopeActive($query)
    {
        return $query->where('status', 0);
    }

    public function books()
    {
    	return $this->hasMany(Book::class);
    }

    public static function nameExist($user_mail)
    {
        $nameExist = Seller::where('username', $user_mail)->first();
        if($nameExist)
        {
            return true;
        }
    }
}
