<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carte extends Model
{
    //
    public function scopeActive($query)
    {
        return $query->where('status', 0);
    }

    public function books()
    {
    	return $this->hasMany(Book::class);
    }
}
