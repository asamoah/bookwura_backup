<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject as Sub;

class SubjectController extends Controller
{
    //
    public function list_all()
    {
    	$ct = Sub::latest()->get();
    	$success_msg = "";
    	return view('back.subject', compact('success_msg', 'ct'));
    }

    public function state_change($id, $eid)
    {	
    	$ct = Sub::find($eid);
    	$ct->status = $id;
    	$ct->save();

    	$ct = Sub::latest()->get();
    	$success_msg = "Update Successful";
    	return view('back.subject', compact('success_msg', 'ct'));
    }

    public function create(Request $request)
    {
    	$this->validate(request(),[
    		'title' => 'required',
    	]);

    	$ct = new Sub();
    	$ct->title = strtoupper($request->title);
    	$ct->save();

    	$ct = Sub::latest()->get();
    	$success_msg = "New Subject Successfully Added";
    	return view('back.subject', compact('success_msg', 'ct'));
    }
}
