<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert as Ads;

class AdvertController extends Controller
{
    //
	public function make_form()
	{
		$success_msg = '';
		return view('back.new-ads', compact('success_msg'));
	}

	public function ads_list()
	{
		$success_msg = '';
		return view('back.ads', compact('success_msg'));
	}
	public function state_change($id,$eid)
	{
		$adv = Ads::find($eid);
		$adv->status = $id;
		$adv->save();
		$success_msg = 'Update Successful';
		return view('back.ads', compact('success_msg'));

	}

	public function create(Request $request)
	{
		$this->validate(request(),[
			'name' => 'required',
			'feat_img' => 'required|max:2000|image|mimes:jpeg,png,bmp'
		]);

		$adv = new Ads();
		$adv->name = strtoupper($request->name);
		if($request->hasFile('feat_img'))
    	{
    		// echo $request->feat_img->extension();
    		$photoName = $request->file('feat_img')->store('adverts','public');
	        $request->feat_img->move('adverts', $photoName);
	        $adv->foto ='/'.$photoName;
    	}
    	$adv->save();

    	$success_msg = 'New Advertisement Successfully Added';
		return view('back.ads', compact('success_msg'));
	}
}
