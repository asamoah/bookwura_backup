<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Mail\UserActivate as ActMail;

class RegisterController extends Controller
{
    //
    public function index(Request $request)
    {
        //        validate the user details
        $this->validate(request(),[
               'name' => 'required',         
               'email' => 'required|email',         
               'password' => 'required|confirmed'         
        ]);
        
        $user_avai = User::isAlreadyExist(request('email'));
        
        if($user_avai)
        {
//            errors()->get();
//            $errors = array('')
            return redirect()->route('back-register')->withErrors('An Account with Such Email Already Exist!!')->withInput();
        
        }
        else
        {
//              create and save user
//            $password = bcrypt(request('password'));
            $user = new User();
            $user->name = $request->name ;
            $user->username = $request->username ;
            $user->email =$request->email ;
            $user->password =bcrypt($request->password);
            $user->save();
   
            // to send the email
      $last_mes = User::latest()->first();
    \Mail::to('kasamoahboateng@gmail.com')->send(new ActMail($last_mes));
    //        
    //        redirect
            $success_msg = 'User Creation Successful !! Please Wait For Your Account to be Activated';
            return view('auth.register',compact('success_msg'));
        }
//      
    }
}
