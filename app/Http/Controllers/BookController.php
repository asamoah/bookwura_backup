<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Book as Bk;

class BookController extends Controller
{
    //
    public function create(Request $request)
    {
    	$this->validate(request(),[
               'title' => 'required',         
               'username' => 'required',        
               'carte' => 'required',        
               'subject' => 'required',        
               'price' => 'required',
               // 'description' => 'required',
               'feat_img' => 'max:2000|image|mimes:jpeg,png,bmp',        
        ]);

        // $usx =  \App\Seller::where('username',strtolower($request->username))->where('passwd',$request->password)->first();
    	$usx =  \App\Seller::where('username',strtolower($request->username))->first();

        // if($usx->count() == 0)
        if(is_null($usx))
        {
            return back()->withErrors('Invalid Username!!')->withInput();
        }
        if($usx->passwd != $request->password)
        {

            return back()->withErrors('Invalid User Credential!!')->withInput();
        }

        $user_id = \App\Seller::where('username',strtolower($request->username))->first()->id;

        $bk = new Bk();
        $bk->name = strtoupper($request->title);
        $bk->description = $request->description;
        $bk->price = $request->price.' GHS' ;

        if($request->hasFile('feat_img'))
	    	{
	    		// echo $request->feat_img->extension();
	    		$photoName = $request->file('feat_img')->store('book-foto','public');
		        $request->feat_img->move('book-foto', $photoName);
		        $bk->foto ='/'.$photoName;
	    	}else
            {

                $bk->foto = 'no_image.jpg';
            }

        $bk->seller_id = $user_id;
        $bk->carte_id = $request->carte;
        $bk->subject_id = $request->subject;
        $bk->save();

        $success_msg = "Thank You. Book Successfully Added!!";
        return view('front.sell', compact('success_msg'));
    }

    public function list_all()
    {
    	$bt = Bk::latest()->get();
    	$success_msg = '';
    	return view('back.book-available', compact('success_msg', 'bt'));
    }

    public function state_change($id, $eid)
    {
    	$bt = Bk::find($eid);
    	$bt->status = $id;
    	$bt->save();

    	$bt = Bk::latest()->get();
    	$success_msg = 'Update Successful';
    	return view('back.book-available', compact('success_msg', 'bt'));
    }

    public function single($id)
    {
    	$bt = Bk::join('sellers','books.seller_id','=','sellers.id')->join('cartes','books.carte_id','=','cartes.id')->join('subjects','books.subject_id', '=', 'subjects.id')->select('books.*','sellers.username as s_uname','cartes.title as c_title', 'subjects.title as s_title')->where('books.id',$id)->first();
    	// return $bt;
    	return response()->json($bt);
    }
    public function sing($id)
    {
    	$bt = Bk::find($id)->join('sellers','books.seller_id','=','sellers.id')->join('cartes','books.carte_id','=','cartes.id')->join('subjects','books.subject_id', '=', 'subjects.id')->select('books.*','sellers.username as s_uname','cartes.title as c_title', 'subjects.title as s_title')->first();
    	// $bt = Bk::find($id);
    	return $bt;
    	// return response()->json($bt);
    }
}
