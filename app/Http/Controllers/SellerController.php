<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seller as Sela;

class SellerController extends Controller
{
    //
    public function list_all()
    {
    	$sel = Sela::latest()->get();
    	$success_msg = '';
    	return view('back.sellers', compact('success_msg','sel'));
    }
    public function state_change($id, $eid)
    {
    	$sel = Sela::find($eid);
    	$sel->status = $id;
    	$sel->save();

    	$sel = Sela::active()->latest()->get();
    	$success_msg = 'Update Succesfully Done';
    	return view('back.sellers', compact('success_msg','sel'));
    }

    public function single($id)
    {
    	$sel = Sela::find($id);
    	return response()->json($sel);
    }

    public function create(Request $request)
    {
    	$this->validate(request(),[
               'name' => 'required',         
               'username' => 'required',         
               'phone' => 'required',         
               'email' => 'email',         
               'password' => 'required|confirmed'         
        ]);

        $user_avai = Sela::nameExist(request('username'));
        
        if($user_avai)
        {
            return back()->withErrors('An Account with Such Username Already Exist!!')->withInput();
        }
        
        else
        {
	        $sel = new Sela();
	        $sel->name = strtoupper($request->name);
	        $sel->username = strtolower($request->username);
	        $sel->passwd = $request->password;
	        $sel->location = $request->location;
	        $sel->password = bcrypt($request->password);
	        $sel->email = $request->email;
	        $sel->phone = $request->phone;
	        $sel->save();

	        $success_msg = "Registeration Succesfully. <a href='".url('/sell')."'>Proceed to Sell Your book</>";
	        return view('front.signup', compact('success_msg')); 
	    }     
    }
}
