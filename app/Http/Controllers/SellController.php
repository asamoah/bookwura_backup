<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book as Bk;

class SellController extends Controller
{
    //
    public function home_search(Request $request)
    {
        $this->validate(request(),[
            'search' => 'required',
        ]);
    	$searc = '%'.$request->search.'%';
    	$bk = $searc;
       $bkk = Bk::where('books.name','LIKE',$searc)->join('sellers','books.seller_id','=','sellers.id')->join('cartes','books.carte_id','=','cartes.id')->join('subjects', 'books.subject_id', '=', 'subjects.id')->select('books.*','sellers.name as sname','sellers.username as s_uname','sellers.phone','sellers.location','cartes.title as c_title', 'subjects.title as s_title')->get();

    	if(count($bkk) ==0 )
    	{
    		return redirect('/home')->withErrors(['no BOOK with Such TITLE Uploaded'])->withInput();
    	}
    	return view('front.result', compact('bk'));
    }

    public function sort_cartegory($id)
    {
        $carte = $id;
        return view('front.show_all_cartegory', compact('carte'));
    }
}
