<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carte as Ct;

class CarteController extends Controller
{
    //
    public function list_all()
    {
    	$ct = Ct::latest()->get();
    	$success_msg = "";
    	return view('back.carte', compact('success_msg', 'ct'));
    }

    public function state_change($id, $eid)
    {	
    	$ct = Ct::find($eid);
    	$ct->status = $id;
    	$ct->save();

    	$ct = Ct::latest()->get();
    	$success_msg = "Update Successful";
    	return view('back.carte', compact('success_msg', 'ct'));
    }

    public function create(Request $request)
    {
    	$this->validate(request(),[
    		'title' => 'required',
    	]);

    	$ct = new Ct();
    	$ct->title = strtoupper($request->title);
    	$ct->save();

    	$ct = Ct::latest()->get();
    	$success_msg = "New Carte Successfully Added";
    	return view('back.carte', compact('success_msg', 'ct'));
    }
}
