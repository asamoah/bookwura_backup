<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User as User;

class LoginController extends Controller
{
    //
        
    public function __construct()
    {
        $this->middleware('guest')->except(['store','destroy']);
    }
    
    public function create()
    {
        return view('auth.login');
    }
    
    public function store()
    {
        //        validate the user details
        $this->validate(request(),[
               'password' => 'required',         
               'email' => 'required|email',     
        ]);
        
        if(! auth()->attempt(request(['email','password'])))
        {

            return redirect()->route('login')->withErrors('Invalid User Credential!!')->withInput();
        }
        
        $active_check = User::where('email',request('email'))->first();
        if($active_check->is_active == 1)
        {
            return redirect()->route('login')->withErrors('Sorry, Your Account is not Active !!')->withInput();
        }
        
        return redirect()->route('backend');
        
    }
    
    public function destroy()
    {
        auth()->logout();
        
        $success_msg = 'Logout Successful';
        return view('auth.login', compact('success_msg'));
    }
}
