<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BackendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['register','login']);
    }
    
    public function index()
    {
        return view('back.dashboard');
    }
    
    public function register()
    {
//        $errors = '';
        $success_msg = '';
        return view('auth.register', compact('success_msg'));
    }
    
    public function login()
    {
        $success_msg = '';
        return view('auth.login', compact('success_msg'));
    }
}
